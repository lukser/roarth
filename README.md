# Roarth

WARNING!!! NOT PRODUCTION READY!

Roarth is a language inspired by many languages like D, Zig, Gleam and Rust. Taking in account languages listed, the result is barely predictable.

## What to expect?

- Compiled to binary
- Statically typed
- Macros are not the answer. Flexible type system is!
- AST is always there when you need it. Create your own syntax if you need it
- hello(a, b) == a.hello(b) == a hello b
- Traits?
- NO GC?! Maybe one day...
- The language might be a bit `lazy` sometimes
- FFI, ABI, etc.

### Roarth is NOT:

- Object Oriented in any way, though the OOP might be achieved using structs, type system and some magic
- The best language in the world (it might me for someone, but still...)
- The answer to which is 42

## Getting Started

### Run

```console
roarth run ./examples/basics.roar
```

### Build

```console
roarth build ./examples/basics.roar -o basics
./basics
```

## Why?

I like Rust, Porth and programming. Why the hell not? Maybe it will not become a mainstream language, but someone could get inspired by Roarth.

## Features

- [x] Basic arithmetic (e.g. `10 30 + .`)
- [ ] Advanced arithmetic (e.g. `40 12 * 3 / 2 - .`)
- [ ] Variables
- [ ] Statements
- [ ] Procedures

## FAQ

### Why new language?

Writing a language is always fun and I gave it a try. Btw the language will evolve and notwithstanding to being inspired by Porth
the syntax might change to something like D + Zig + Gleam. Nobody knows.

The only thing is expected is statically typed procedural language with good enough flexibility and safety.
And I like modifying AST on the fly, monads, comptime and being a bit... functional. ^_^



### Roarth, seriously?

The Roarth initially is a combination of Rust and Forth, but then evolved to a self-contained name.
It might bite you if your code is not safe enough.
Roar! :D

### Why would I need it?

If you ask, then you don't. Maybe something like programming adventures. ;)

### 

## License

MIT license.

## Contribution

Look for issues and give comments or [buy me some coffee](https://ko-fi.com/lukser).
