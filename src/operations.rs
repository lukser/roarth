use std::str::FromStr;

use crate::lexer::FileToken;

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum Operation {
    Push,
    Plus,
    Minus,
    Dump,
    Equal,
    If,
    End,
}

pub type Argument = i32;

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Action {
    pub operation: Operation,
    pub arguments: Vec<Argument>,
}

impl Action {
    pub fn new(op: Operation, args: Vec<Argument>) -> Self {
        Self {
            operation: op,
            arguments: args,
        }
    }
}

impl FromStr for Action {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let result = match s {
            "+" => Self::plus(),
            "-" => Self::minus(),
            "." => Self::dump(),
            "=" => Self::equal(),
            "if" => Self::if_op(),
            "end" => Self::end(),
            _ => Self::push(
                s.parse::<i32>()
                    .unwrap_or_else(|_| panic!("Undefined syntax")),
            ),
        };

        Ok(result)
    }
}

impl From<FileToken> for Action {
    fn from(ft: FileToken) -> Self {
        let result = match ft.token.as_str() {
            "+" => Self::plus(),
            "-" => Self::minus(),
            "." => Self::dump(),
            "=" => Self::equal(),
            "if" => Self::if_op(),
            "end" => Self::end(),
            _ => Self::push(
                ft.token.as_str()
                    .parse::<i32>()
                    .unwrap_or_else(|e| panic!(
                        "{}:{}:{}: {}",
                        ft.file_name,
                        ft.position.0,
                        ft.position.1,
                        e
                    )),
            ),
        };

        result
    }
}

pub trait StackActions {
    fn push(arg: Argument) -> Action;
    fn dump() -> Action;
}

pub trait FlowActions {
    fn if_op() -> Action;
    fn end() -> Action;
}

pub trait MathActions {
    fn plus() -> Action;
    fn minus() -> Action;
    fn equal() -> Action;
}

impl StackActions for Action {
    fn push(arg: Argument) -> Action {
        Action::new(Operation::Push, vec![arg])
    }

    fn dump() -> Action {
        Action::new(Operation::Dump, vec![])
    }
}

impl FlowActions for Action {
    fn if_op() -> Action {
        Action::new(Operation::If, vec![])
    }

    fn end() -> Action {
        Action::new(Operation::End, vec![])
    }
}

impl MathActions for Action {
    fn plus() -> Action {
        Action::new(Operation::Plus, vec![])
    }

    fn minus() -> Action {
        Action::new(Operation::Minus, vec![])
    }

    fn equal() -> Action {
        Action::new(Operation::Equal, vec![])
    }
}

