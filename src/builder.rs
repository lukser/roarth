use std::{io::Write, process::Command, str};
use std::borrow::BorrowMut;
use std::cell::RefCell;
use std::rc::Rc;
use std::str::FromStr;

use crate::lexer::{Lexer, Tokenizer};
use crate::operations::{Action, Argument, Operation};

#[derive(Clone, Default, Debug, Eq, PartialEq)]
pub struct Program {
    actions: Vec<RefCell<Action>>,
}

impl Program {
    pub fn simulate(&mut self) {
        self.crossref();
        let mut stack: Vec<Argument> = Vec::new();

        let mut ip = 0;
        while ip < self.actions.len() {
            let action = self.actions
                .get_mut(ip)
                .expect("No operations at instruction pointer")
                .get_mut();

            match action.operation {
                Operation::Push => {
                    stack.push(action.arguments[0]);
                }
                Operation::Plus => {
                    let values = stack.iter().rev().take(2).sum();
                    stack.push(values);
                }
                Operation::Minus => {
                    let first = stack.pop().expect("Not enough values in the stack");
                    let second = stack.pop().expect("Not enough values in the stack");
                    stack.push(second - first);
                }
                Operation::Dump => {
                    let value = stack.pop().expect("Not enough values in the stack");
                    println!("{}", value);
                }
                Operation::If => {
                    let first = stack.pop().expect("Not enough values in the stack");
                    if first == 0 {
                        if action.arguments.len() >= 2 { panic!(); }
                        let next_ip = action.arguments
                            .first()
                            .expect("No reference to 'end'");
                        ip = *next_ip as usize;
                    }
                }
                Operation::End => {}
                Operation::Equal => {
                    let first = stack.pop().expect("Not enough values in stack");
                    let second = stack.pop().expect("Not enough values in stack");
                    stack.push(first.eq(&second as &i32) as Argument);
                }
            }

            ip += 1;
        }
    }


    pub fn crossref(&mut self) {
        let mut stack: Vec<Argument> = vec![];
        for (i, action) in self.actions.clone().iter().enumerate() {
            match action.borrow_mut().operation {
                Operation::If => {
                    stack.push(i as Argument);
                }
                Operation::End => {
                    let if_addr = stack.pop().expect("Not enough values in the stack");
                    if let Some(p) = self.actions.get_mut(if_addr as usize) {
                        p.borrow_mut().get_mut().arguments.push(i as Argument);
                    }

                    if let Some(p) = self.actions.get_mut(i) {
                        p.borrow_mut().get_mut().arguments.push(if_addr);
                    }
                }
                _ => {}
            }
        }
    }

    pub fn compile(&mut self, output_file: &str) {
        let mut output = std::fs::File::create(format!("{}.asm", output_file))
            .unwrap_or_else(|_| panic!("Could not create file '{}'", output_file));

        output
            .write_all(self.to_asm().as_bytes())
            .expect("failed to write to file");

        let _ = Command::new("nasm")
            .args([
                "-o",
                format!("{}.o", output_file).as_str(),
                "-f",
                "elf64",
                format!("{}.asm", output_file).as_str(),
            ])
            .output()
            .expect("failed to execute 'nasm'");

        let _ = Command::new("ld")
            .args(["-o", output_file, format!("{}.o", output_file).as_str()])
            .output()
            .expect("failed to execute process");
    }

    pub fn from_file(file_path: &str) -> Program {
        let input = Tokenizer::lex_file(file_path.to_string());
        let actions = input.iter()
            .map(|ft| RefCell::new(Action::from(ft.clone())))
            .collect();
        Program { actions }
    }
}

trait AsmTranspiler {
    fn to_asm(&self) -> String;
}

impl AsmTranspiler for Program {
    fn to_asm(&self) -> String {
        let mut buffer = String::from("segment .text\n");
        buffer.push_str(concat!(
        "dump:\n",
        "    mov     r9, -3689348814741910323\n",
        "    sub     rsp, 40\n",
        "    mov     BYTE [rsp+31], 10\n",
        "    lea     rcx, [rsp+30]\n",
        ".L2:\n",
        "    mov     rax, rdi\n",
        "    lea     r8, [rsp+32]\n",
        "    mul     r9\n",
        "    mov     rax, rdi\n",
        "    sub     r8, rcx\n",
        "    shr     rdx, 3\n",
        "    lea     rsi, [rdx+rdx*4]\n",
        "    add     rsi, rsi\n",
        "    sub     rax, rsi\n",
        "    add     eax, 48\n",
        "    mov     BYTE [rcx], al\n",
        "    mov     rax, rdi\n",
        "    mov     rdi, rdx\n",
        "    mov     rdx, rcx\n",
        "    sub     rcx, 1\n",
        "    cmp     rax, 9\n",
        "    ja      .L2\n",
        "    lea     rax, [rsp+32]\n",
        "    mov     edi, 1\n",
        "    sub     rdx, rax\n",
        "    xor     eax, eax\n",
        "    lea     rsi, [rsp+32+rdx]\n",
        "    mov     rdx, r8\n",
        "    mov     rax, 1\n",
        "    syscall\n",
        "    add     rsp, 40\n",
        "    ret\n",
        ));
        buffer.push_str(concat!("global _start\n", "_start:\n"));

        for action in self.actions.iter() {
            let asm = match action.borrow().operation {
                Operation::Push => format!("    push {}\n", action.borrow().arguments[0]),
                Operation::Plus => concat!(
                "    pop rax\n",
                "    pop rbx\n",
                "    add rax, rbx\n",
                "    push rax\n",
                )
                    .to_string(),
                Operation::Minus => concat!(
                "    pop rax\n",
                "    pop rbx\n",
                "    sub rbx, rax\n",
                "    push rbx\n",
                )
                    .to_string(),
                Operation::Dump => concat!("    pop rdi\n", "    call dump\n").to_string(),
                Operation::Equal => concat!(
                "    mov rcx, 0\n",
                "    mov rdx, 1\n",
                "    pop rax\n",
                "    pop rbx\n",
                "    cmp rax, rbx\n",
                "    cmove rcx, rdx\n"
                ).to_string(),
                Operation::If => "".to_string(),
                Operation::End => "".to_string(),
            };
            buffer.push_str(&asm);
        }

        buffer.push_str(concat!(
        "    mov rax, 60\n",
        "    mov rdi, 0\n",
        "    syscall\n",
        ));
        buffer
    }
}

impl FromStr for Program {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let actions = s
            .split_whitespace()
            .into_iter()
            .map(|ch| RefCell::new(Action::from_str(ch).unwrap()))
            .collect();
        Ok(Program { actions })
    }
}

#[cfg(test)]
mod tests {
    use crate::operations::{MathActions, StackActions};

    use super::*;

    #[test]
    fn transpile_to_asm_empty_input() {
        let program: Program = Program::default();
        assert_eq!(
            program.to_asm(),
            concat!(
            "segment .text\n",
            "dump:\n",
            "    mov     r9, -3689348814741910323\n",
            "    sub     rsp, 40\n",
            "    mov     BYTE [rsp+31], 10\n",
            "    lea     rcx, [rsp+30]\n",
            ".L2:\n",
            "    mov     rax, rdi\n",
            "    lea     r8, [rsp+32]\n",
            "    mul     r9\n",
            "    mov     rax, rdi\n",
            "    sub     r8, rcx\n",
            "    shr     rdx, 3\n",
            "    lea     rsi, [rdx+rdx*4]\n",
            "    add     rsi, rsi\n",
            "    sub     rax, rsi\n",
            "    add     eax, 48\n",
            "    mov     BYTE [rcx], al\n",
            "    mov     rax, rdi\n",
            "    mov     rdi, rdx\n",
            "    mov     rdx, rcx\n",
            "    sub     rcx, 1\n",
            "    cmp     rax, 9\n",
            "    ja      .L2\n",
            "    lea     rax, [rsp+32]\n",
            "    mov     edi, 1\n",
            "    sub     rdx, rax\n",
            "    xor     eax, eax\n",
            "    lea     rsi, [rsp+32+rdx]\n",
            "    mov     rdx, r8\n",
            "    mov     rax, 1\n",
            "    syscall\n",
            "    add     rsp, 40\n",
            "    ret\n",
            "global _start\n",
            "_start:\n",
            "    mov rax, 60\n",
            "    mov rdi, 0\n",
            "    syscall\n",
            )
        );
    }

    #[test]
    fn parse_from_str_simple() {
        let program = Program::from_str("34 35 + .").unwrap();
        let expected = [Action::push(34), Action::push(35), Action::plus(), Action::dump()];
        for (action, expect) in program.actions.iter().zip(expected) {
            assert_eq!(action.borrow_mut().clone(), expect);
        }
    }

    #[test]
    fn test_crossref() {
        let mut program = Program::from_str("34 35 + 69 = if\n420 .\nend").unwrap();
        println!("{:?}", program.actions);

        program.crossref();

        println!("{:?}", program.actions);
    }
}
