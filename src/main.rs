mod builder;
mod lexer;
mod operations;
mod utils;

use clap::{arg, command, Arg, Command};
use crate::builder::Program;

fn main() {
    let matches = command!()
        .subcommand_required(true)
        .arg_required_else_help(true)
        // Run subcommand
        .subcommand(
            Command::new("run")
                .about("Run interpretetor on file")
                .arg(arg!(<INPUT> "Path to the input file")),
        )
        // Build subcommand
        .subcommand(
            Command::new("build").about("Compile the programm").args(&[
                arg!(<INPUT> "Path to the input file"),
                Arg::new("output")
                    .short('o')
                    .long("output")
                    .help("Path to the output file")
                    .takes_value(true),
            ]),
        )
        .get_matches();

    match matches.subcommand() {
        Some(("run", run_matches)) => {
            let input = run_matches.value_of("INPUT").unwrap();
            let mut program = Program::from_file(input);
            program.simulate();
        }
        Some(("build", build_matches)) => {
            let input = build_matches.value_of("INPUT").unwrap();
            let output = build_matches.value_of("output").unwrap();
            let mut program = Program::from_file(input);
            program.compile(output);
        }
        _ => unreachable!(),
    }
}
