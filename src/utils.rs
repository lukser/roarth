pub fn addr_of(s: &str) -> usize {
    s.as_ptr() as usize
}

pub fn split_whitespace_indices(s: &str) -> Vec<(usize, String)> {
    s.split_whitespace()
        .map(move |sub| (addr_of(sub) - addr_of(s), sub.to_string()))
        .collect()
}