use std::fs::File;
use std::io::{BufReader, Read};
use std::path::Path;

use crate::utils::split_whitespace_indices;

pub type Token = String;

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct FileToken {
    pub token: Token,
    pub position: (usize, usize),
    pub file_name: String,
}

pub trait Lexer {
    fn lex_string(input: String) -> Vec<FileToken>;
    fn lex_file(file_path: String) -> Vec<FileToken>;
}

pub struct Tokenizer;

impl Tokenizer {
    fn lex_line(line: String) -> Vec<(usize, Token)> {
        split_whitespace_indices(line.as_str())
    }

    fn tokenize(input: String, origin: String) -> Vec<FileToken> {
        let mut tokens: Vec<FileToken> = vec![];
        for (row, line) in input.lines().enumerate() {
            let mut line_tokens: Vec<FileToken> = Self::lex_line(line.to_string())
                .iter()
                .map(|(col, token)| FileToken {
                    token: token.to_string(),
                    position: (row, *col),
                    file_name: origin.clone(),
                })
                .collect();
            tokens.append(&mut line_tokens);
        }
        tokens
    }
}

impl Lexer for Tokenizer {
    fn lex_string(input: String) -> Vec<FileToken> {
        Self::tokenize(input, "none".to_string())
    }

    fn lex_file(file_path: String) -> Vec<FileToken> {
        let path = Path::new(file_path.as_str());
        let file = File::open(&path).unwrap();
        let mut buf_reader = BufReader::new(file);
        let mut contents = String::new();
        buf_reader.read_to_string(&mut contents).unwrap();
        Self::tokenize(contents, file_path)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn check_lex_line() {
        let expected = vec![(0, "30"), (4, "21"), (10, "+"), (15, ".")];
        let result = Tokenizer::lex_line("30  21    +    .".to_string());

        for (r, e) in result.iter().zip(expected) {
            assert_eq!(r.0, e.0);
            assert_eq!(r.1, e.1);
        }
    }

    #[test]
    fn check_lex_file() {
        let result = Tokenizer::lex_file("examples/basics.roar".to_string());
        println!("{:?}", result);
    }
}
